/*
 * main.h
 *
 *  Created on: 8 lut 2021
 *      Author: ciame
 */

#ifndef MAIN_H_

#define DEBUG 0
#define RELAY_ON_DELAY 50
#define WATCHDOG_RESET_TIME 5
#define MRF24J_MAX_DELAY_TIME 40
#define MIN_VERY_FAST 100
#define MIN_FAST 25
#define MIN_MEDIUM 5
#define MIN_LOW 2

void init_INT0 (void);
void init_Timers (void);
void reset_watchdog_counter (void);
void mrf24j_receive_data (void);
void run_forward (void);
void run_reverse (void);
void stop_motor (void);
void toggle_lamp (void);
void mrf24j_refresh(void);


typedef struct
{
	uint8_t volatile forward;
	uint8_t volatile reverse;
	uint8_t volatile watchdog;
	uint8_t volatile mrf24j_receive;

}TIM;


#define MAIN_H_



#endif /* MAIN_H_ */
