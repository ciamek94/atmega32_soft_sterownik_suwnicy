# Sterownik suwnicy

## Opis działania

Zaprojektowany układ elektroniczny służy do sterowania pracą suwnicy (dźwigu). Sterownik służy do interpretacji danych otrzymanych bezprzewodowo z pilota w oparciu o transmisję bezprzewodową w paśmie częstotliwości 2,4GHz w standardze IEEE 802.15.4 (Zigbee) z wykorzystaniem modułu firmy Microchip MRF24J40MA. Na ich podstawie układ steruje pracą przekaźników podłączonych do wejść przemiennika częstotliwości (zmiana kierunku obrotów silnika), który steruje pracą silnika asynchronicznego o mocy 1,5kW sprzęgniętego z przekładnią ślimakową o przełożeniu 1:150. Prędkość obrotowa silnika regulowana jest przez sterownik poprzez regulację napięcia (zmiana współczynnika wypełnienia) podawanego na odpowiednie wejście przetwornicy. Układ zapewnia pełną izolację galwaniczną pomiędzy sterownikiem, a przemiennikiem częstotliwości zrealizowaną z wykorzystaniem optotranzystorów. Stosowanie przetwornicy częstotliwości (falownika) zaprogramowanej do pracy wektorowej z silnikiem asynchronicznym zapewnia stałość momentu obrotowego w pełnym zakresie regulacji prędkości obrotowej od zera do prędkości znamionowej silnika co ważne jest dla urządzeń o dźwigowej charakterystyce momentu obciążenia.

Sterownik otrzymuje co pewien okres ramkę danych z pilota na podstawie, której jest w stanie stwierdzić jaką prędkość, kierunek oraz stan dodatkowego przekaźnika sterującego pracą naświetlacza LED wybrał użytkownik. Stan przekaźnika zapisywany jest do pamięci nieulotnej EEPROM przez co po utracie zasilania układ pamięta poprzedni stan.

![](./crane_images/IMG_20210425_154949706.jpg )
![](./crane_images/IMG_20210425_154957098_HDR.jpg )
![](./crane_images/IMG_20210207_165013515.jpg)
![](./crane_images/IMG_20210207_164944555.jpg)
![](./crane_images/IMG_20210425_181958963.jpg )
