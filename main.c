#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/atomic.h>


#include "MRF24J/mrf24j.h"
#include "IO/io.h"
#include "main.h"

#if DEBUG == 1
#include "MKUART/mkuart.h"
#endif

TIM Timer;

enum {stop, forward, reverse, nope, low = 0, medium = 1, fast = 2, very_fast = 3};
uint8_t state = stop;

short int volatile scaled_speed;
uint8_t volatile pwm;
uint8_t level;
uint8_t last_led_state;
uint8_t divide[4] = {20, 15, 4, 2};

int main (void)
{
	wdt_enable(WDTO_120MS);
	#if DEBUG == 1
	USART_Init( __UBRR );			// inicjalizacja UART
	#endif

	wdt_reset();
	init_INT0();
	init_Timers();
	pin_init();
	sei();

	//MRF24j Init
	mrf24j_SPI_init();
	mrf24j_reset();
	mrf24j_rx_flush();
	mrf24j_init();
	mrf24j_set_pan(PAN_ADDRESS);
	mrf24j_address16_write(CRANE_CONTROLLER_ADREESS);
	wdt_reset();

	#if DEBUG == 1
	uart_puts("INIT");
	#endif

	while(1)
	{
		reset_watchdog_counter();
		mrf24j_receive_data();
		toggle_lamp();
	}
}

void init_INT0 (void)
{
	DDRD &= ~(1<<PD2);
	PORTD |= (1<<PD2);
	MCUCR |= (1<<ISC01);
	//GICR |= (1<<INT0);
}

void init_Timers (void)
{
	//Timer 0  10ms
	TCCR0 |= (1<<WGM01) | (1<<CS02) | (1<<CS00);
	TIMSK |= (1<<OCIE0);
	OCR0 = 77;

	//Timer 2
	TCCR2 |= (1<<WGM21) | (1<<CS21); //CTC prescaler 1
	TIMSK |= (1<<OCIE2);
	OCR2 = 39;
}

void reset_watchdog_counter (void)
{
	if(!Timer.watchdog)
	{
		Timer.watchdog = WATCHDOG_RESET_TIME;
		wdt_reset();
	}
}

void mrf24j_receive_data (void)
{
	if (GIFR & (1<<INTF0))
	{
		mrf24j_interrupt_handler(&rx_info, &tx_info);
		Timer.mrf24j_receive = MRF24J_MAX_DELAY_TIME;

		ATOMIC_BLOCK(ATOMIC_FORCEON)
		{
			scaled_speed = (short int)(rx_info.rx_data[2] << 8) + (short int)rx_info.rx_data[3];
			level = rx_info.rx_data[1];
			if (scaled_speed < 0) pwm = - scaled_speed / divide[level];
			else pwm = scaled_speed / divide[level];
			if (pwm > 250) pwm = 255;
		}

		#if DEBUG == 1
		uart_putint(level,10);
		uart_puts(" ");
		uart_putint(scaled_speed,10);
		uart_puts(" ");
		uart_putint(pwm,10);
		uart_puts(" \n\r");
		#endif

		GIFR |= (1<<INTF0);
	}

	if(!Timer.mrf24j_receive)
	{
		scaled_speed = 0;
		rx_info.rx_data[2] = 0;
		rx_info.rx_data[3] = 0;
		Timer.mrf24j_receive = MRF24J_MAX_DELAY_TIME;
		//mrf24j_refresh();
	}

	if (scaled_speed > MIN_VERY_FAST) run_forward();
	else if (scaled_speed < -MIN_VERY_FAST) run_reverse();
	else stop_motor();
}

void run_forward (void)
{
	if(state != forward)
	{
		ATOMIC_BLOCK(ATOMIC_FORCEON)
		{
			Timer.forward = RELAY_ON_DELAY;
			Timer.reverse = 0;
			RELAY2_OFF;
			RELAY1_OFF;
		}
	}
	state = forward;
}

void run_reverse (void)
{
	if(state != reverse)
	{
		ATOMIC_BLOCK(ATOMIC_FORCEON)
		{
			Timer.reverse = RELAY_ON_DELAY;
			Timer.forward = 0;
			RELAY2_OFF;//RELAY2_ON; //dla sterownia z przekačnikiem do wyboru kierunku
			RELAY1_OFF;
		}
	}
	state = reverse;
}

void stop_motor(void)
{
	RELAY2_OFF;
	RELAY1_OFF;
	Timer.reverse = 0;
	Timer.forward = 0;
	state = stop;
}

void toggle_lamp (void)
{
	if(rx_info.rx_data[0]) RELAY3_ON;
	else RELAY3_OFF;
}


/*void mrf24j_refresh(void)
{
	if(mrf24j_flag.reset_flag)
	{
		tx_info.channel_busy = 0;
		mrf24j_flag.reset_flag = 0;
		mrf24j_reset();
		mrf24j_rx_flush();
		mrf24j_init();
		mrf24j_set_pan(PAN_ADDRESS);
		mrf24j_address16_write(CRANE_CONTROLLER_ADREESS);
	}
}*/

ISR (TIMER0_COMP_vect)
{
	uint8_t n;
	n = Timer.forward;		/* 100Hz Timer */
	if (n)
	{
		Timer.forward = --n;
		if (!Timer.forward) RELAY1_ON;
	}

	n = Timer.reverse;		/* 100Hz Timer */
	if (n)
	{
		Timer.reverse = --n; /* 100Hz Timer */
		if (!Timer.reverse)
		{
			RELAY2_ON;
			//RELAY1_ON; //dla sterownia z przekačnikiem do wyboru kierunku
		}
	}

	n = Timer.watchdog;		/* 100Hz Timer */
	if (n) Timer.watchdog = --n;

	n = Timer.mrf24j_receive;		/* 100Hz Timer */
	if (n) Timer.mrf24j_receive = --n;
}

ISR(TIMER2_COMP_vect)
{
	static uint8_t cnt = 0;
	if(cnt <= pwm) PWM_ON; else PWM_OFF;
	cnt++;
}

