
#include "io.h"

#include <avr/io.h>
#include "../main.h"


void pin_init (void)
{
	RELAY_PORT |= RELAY1_PIN | RELAY2_PIN | RELAY3_PIN;
	RELAY_DIR |= RELAY1_PIN | RELAY2_PIN | RELAY3_PIN;

	PWM_DIR |= PWM_PIN;
	PWM_OFF;

	//Set doesnt used pins as input with pullup
	DDRA = 0x00; // All pins as inputs
	PORTA |= 0xFF;	//Pull up resistors

	DDRB &= ~((1<<PB0) | (1<<PB1) | (1<<PB2) | (1<<PB3));
	PORTB |= ((1<<PB0) | (1<<PB1) | (1<<PB2) | (1<<PB3));

	DDRC &= ~((1<<PC4) | (1<<PC5) | (1<<PC6) | (1<<PC7));
	PORTC |= ((1<<PC4) | (1<<PC5) | (1<<PC6) | (1<<PC7));

	DDRD &= ~((1<<PD3) | (1<<PD4) | (1<<PD5) | (1<<PD6) | (1<<PD7));
	PORTD |= ((1<<PD3) | (1<<PD4) | (1<<PD5) | (1<<PD6) | (1<<PD7));

	#if DEBUG == 0
	DDRD &= ~((1<<PD0) | (1<<PD1));
	PORTD |= ((1<<PD0) | (1<<PD1));
	#endif
}
