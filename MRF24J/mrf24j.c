/*
 * mrf24j.c
 *
 *  Created on: 19 maj 2020
 *      Author: ciame
 */
#include "mrf24j.h"
#include <util/delay.h>
#include <avr/interrupt.h>


// aMaxPHYPacketSize = 127, from the 802.15.4-2006 standard.
//static uint8_t rx_buf[20];

// essential for obtaining the data frame only
// bytes_MHR = 2 Frame control + 1 sequence number + 2 panid + 2 shortAddr Destination + 2 shortAddr Source

const volatile uint8_t bytes_nodata = BYTES_MHR + BYTES_FCS; // no_data bytes in PHY payload,  header length + FCS

//static uint8_t ignoreBytes = 0; // bytes to ignore, some modules behaviour.

//static uint8_t bufPHY = 0; // flag to buffer all bytes in PHY Payload, or not

volatile uint8_t flag_got_rx;
volatile uint8_t flag_got_tx;

rx_info_t rx_info;
tx_info_t tx_info;
//mrf_flag mrf24j_flag;

void mrf24j_SPI_init(void)
{
	/* ustawienie kierunku wyj�ciowego dla linii MOSI, SCK i CS */
	DDRB |= MOSI | SCK | SS;
	/* aktywacja  SPI, tryb pracy Master, pr�dko�� zegara Fosc/2 */
	SPCR |= (1<<SPE)|(1<<MSTR)|(1<<SPI2X);
}

uint8_t mrf24j_SPI_transfer(uint8_t bajt)
{
	SPDR = bajt;
	while(!(SPSR & (1<<SPIF)));
	return SPDR;
}


static inline uint8_t mrf24j_read_short(uint8_t address)
{
	SS_LOW;
    mrf24j_SPI_transfer((address<<1) & 0x7E);
    uint8_t ret = mrf24j_SPI_transfer(0x00);
    SS_HIGH;
    return ret;
}

static inline uint8_t mrf24j_read_long(uint16_t address)
{
	SS_LOW;
    uint8_t ahigh = ((uint8_t)(address >> 3) & 0x7F) | 0x80;
    uint8_t alow = ((uint8_t)(address << 5)) & 0xE0;
    mrf24j_SPI_transfer(ahigh);  // high bit for long
    mrf24j_SPI_transfer(alow);
    uint8_t ret =  mrf24j_SPI_transfer(0);
    SS_HIGH;
    return ret;
}


static inline void mrf24j_write_short(uint8_t address, uint8_t data)
{
	SS_LOW;
    // 0 for top short address, 1 bottom for write
	mrf24j_SPI_transfer(((address<<1) & 0x7E) | 0x01);
	mrf24j_SPI_transfer(data);
	SS_HIGH;
}

static inline void mrf24j_write_long(uint16_t address, uint8_t data)
{
	SS_LOW;
    uint8_t ahigh = ((uint8_t)(address >> 3) & 0x7F) | 0x80;
    uint8_t alow = ((uint8_t)(address << 5) & 0xE0) | 0x10;
    mrf24j_SPI_transfer(ahigh);  // high bit for long
    mrf24j_SPI_transfer(alow);  // last bit for write
    mrf24j_SPI_transfer(data);
    SS_HIGH;
}

void mrf24j_reset(void)
{
	mrf24j_write_short(MRF_SOFTRST, 0x07); // from manual
	while ((mrf24j_read_short(MRF_SOFTRST) & 0x07 ) != 0) ; // wait for soft reset to finish
}

void mrf24j_PA_LNA_enable (uint8_t enable)
{
	if(enable) mrf24j_write_long(MRF_TESTMODE, 0x0f);
	else mrf24j_write_long(MRF_TESTMODE, 0x08);
}

uint16_t mrf24j_get_pan(void)
{
	uint8_t panh = mrf24j_read_short(MRF_PANIDH);
    return panh << 8 | mrf24j_read_short(MRF_PANIDL);
}

void mrf24j_set_pan(uint16_t panid)
{
	mrf24j_write_short(MRF_PANIDH, panid >> 8);
	mrf24j_write_short(MRF_PANIDL, panid & 0xff);
}

void mrf24j_address16_write(uint16_t address16)
{
	mrf24j_write_short(MRF_SADRH, address16 >> 8);
	mrf24j_write_short(MRF_SADRL, address16 & 0xff);
}

uint16_t mrf24j_address16_read(void)
{
    uint16_t a16h = mrf24j_read_short(MRF_SADRH);
    return a16h << 8 | mrf24j_read_short(MRF_SADRL);
}

/**
 * Simple send 16, with acks, not much of anything.. assumes src16 and local pan only.
 * @param data
 */
void mrf24j_send(uint16_t dest16, char * data, uint8_t length)
{
    uint8_t i = 0;
    mrf24j_write_long(i++, BYTES_MHR); // header length
    // +ignoreBytes is because some module seems to ignore 2 bytes after the header?!.
    // default: ignoreBytes = 0;
    mrf24j_write_long(i++, BYTES_MHR + length);

    // 0 | pan compression | ack | no security | no data pending | data frame[3 bits]
    mrf24j_write_long(i++, 0x61); // first byte of Frame Control
    // 16 bit source, 802.15.4 (2003), 16 bit dest,
    mrf24j_write_long(i++, 0x88); // second byte of frame control
    mrf24j_write_long(i++, 1);  // sequence number 1

    mrf24j_write_long(i++, PAN_ADDRESS & 0xff);  // dest panid
    mrf24j_write_long(i++, PAN_ADDRESS >> 8);
    mrf24j_write_long(i++, dest16 & 0xff);  // dest16 low
    mrf24j_write_long(i++, dest16 >> 8); // dest16 high

    mrf24j_write_long(i++, CRANE_CONTROLLER_ADREESS & 0xff); // src16 low
    mrf24j_write_long(i++, CRANE_CONTROLLER_ADREESS >> 8); // src16 high

    // All testing seems to indicate that the next two bytes are ignored.
    //2 bytes on FCS appended by TXMAC
    for (int q = 0; q < length; q++)
    {
    	mrf24j_write_long(i++, *data++);
    }
    // ack on, and go!
    mrf24j_write_short(MRF_TXNCON, (1<<MRF_TXNACKREQ | 1<<MRF_TXNTRIG));
}

void mrf24j_set_interrupts(void)
{
    // interrupts for rx and tx normal complete
	mrf24j_write_short(MRF_INTCON, 0xF6);
}

/** use the 802.15.4 channel numbers..
 */
void mrf24j_set_channel(uint8_t channel)
{
	mrf24j_write_long(MRF_RFCON0, (((channel - 11) << 4) | 0x03));
}

void mrf24j_init(void)
{
	mrf24j_write_short(MRF_PACON2, 0x98); // � Initialize FIFOEN = 1 and TXONTS = 0x6.
	mrf24j_write_short(MRF_TXSTBL, 0x95); // � Initialize RFSTBL = 0x9.

	mrf24j_write_long(MRF_RFCON0, 0x03); // � Initialize RFOPT = 0x03.
	mrf24j_write_long(MRF_RFCON1, 0x02); // � Initialize VCOOPT = 0x02.
	mrf24j_write_long(MRF_RFCON2, 0x80); // � Enable PLL (PLLEN = 1).
	mrf24j_write_long(MRF_RFCON6, 0x90); // � Initialize TXFIL = 1 and 20MRECVR = 1.
	mrf24j_write_long(MRF_RFCON7, 0x80); // � Initialize SLPCLKSEL = 0x2 (100 kHz Internal oscillator).
	mrf24j_write_long(MRF_RFCON8, 0x10); // � Initialize RFVCO = 1.
	mrf24j_write_long(MRF_SLPCON1, 0x21); // � Initialize CLKOUTEN = 1 and SLPCLKDIV = 0x01.

    //  Configuration for nonbeacon-enabled devices (see Section 3.8 �Beacon-Enabled and
    //  Nonbeacon-Enabled Networks�):
	mrf24j_write_short(MRF_BBREG2, 0x80); // Set CCA mode to ED
	mrf24j_write_short(MRF_CCAEDTH, 0x60); // � Set CCA ED threshold.
	mrf24j_write_short(MRF_BBREG6, 0x40); // � Set appended RSSI value to RXFIFO.
	mrf24j_set_interrupts();
	mrf24j_set_channel(12);
	mrf24j_PA_LNA_enable(1);
    // max power is by default.. just leave it...
    // Set transmitter power - See �REGISTER 2-62: RF CONTROL 3 REGISTER (ADDRESS: 0x203)�.
	mrf24j_write_short(MRF_RFCTL, 0x04); //  � Reset RF state machine.
	mrf24j_write_short(MRF_RFCTL, 0x00); // part 2
    _delay_us(200); // delay at least 192usec
}

/**
 * Call this from within an interrupt handler connected to the MRFs output
 * interrupt pin.  It handles reading in any data from the module, and letting it
 * continue working.
 * Only the most recent data is ever kept.
 */
void mrf24j_interrupt_handler(rx_info_t * rx, tx_info_t * tx)
{
    uint8_t last_interrupt = mrf24j_read_short(MRF_INTSTAT);
    if (last_interrupt & MRF_I_RXIF)
    {
        flag_got_rx = 1;
        // read out the packet data...
        uint8_t sreg = SREG;
        cli();
        mrf24j_rx_disable();
        // read start of rxfifo for, has 2 bytes more added by FCS. frame_length = m + n + 2
        uint8_t frame_length = mrf24j_read_long(0x300);

        // from (0x301 + bytes_MHR) to (0x301 + frame_length - bytes_nodata - 1)
        for (int i = 0; i < (frame_length - bytes_nodata); i++)
        {
        	rx -> rx_data[i] = mrf24j_read_long(0x301 + BYTES_MHR + i);
        }

        rx -> frame_length = frame_length;
        // same as datasheet 0x301 + (m + n + 2) <-- frame_length
        rx -> lqi = mrf24j_read_long(0x301 + frame_length);
        // same as datasheet 0x301 + (m + n + 3) <-- frame_length + 1
        rx -> rssi = mrf24j_read_long(0x301 + frame_length + 1);
        mrf24j_rx_flush();
        mrf24j_rx_enable();
        SREG = sreg;
    }
    else mrf24j_rx_flush();

    if (last_interrupt & MRF_I_TXNIF)
    {
        flag_got_tx = 1;
        uint8_t tmp = mrf24j_read_short(MRF_TXSTAT);
        // 1 means it failed, we want 1 to mean it worked.
        tx -> tx_ok = !(tmp & ~(1 << TXNSTAT));
        tx -> retries = tmp >> 6;
        tx -> channel_busy = (tmp & (1 << CCAFAIL));
    }
}

void mrf24j_rx_flush(void)
{
	mrf24j_write_short(MRF_RXFLUSH, 0x01);
}

void mrf24j_rx_disable(void)
{
	mrf24j_write_short(MRF_BBREG1, 0x04);  // RXDECINV - disable receiver
}

void mrf24j_rx_enable(void)
{
	mrf24j_write_short(MRF_BBREG1, 0x00);  // RXDECINV - enable receiver
}

/*
//Call this function periodically, it will invoke your nominated handlers

void mrf24j_check_flags(void (*rx_handler)(void), void (*tx_handler)(void))
{
    // TODO - we could check whether the flags are > 1 here, indicating data was lost?
    if (flag_got_rx) {
        flag_got_rx = 0;
        rx_handler();
    }
    if (flag_got_tx) {
        flag_got_tx = 0;
        tx_handler();
    }
}


//Set RX mode to promiscuous, or normal

void mrf24j_set_promiscuous(uint8_t enabled)
{
    if (enabled)
    {
    	mrf24j_write_short(MRF_RXMCR, 0x01);
    } else
    {
    	mrf24j_write_short(MRF_RXMCR, 0x00);
    }
}

rx_info_t * mrf24j_get_rxinfo(void)
{
    return &rx_info;
}

tx_info_t * mrf24j_get_txinfo(void)
{
    return &tx_info;
}

uint8_t * mrf24j_get_rxbuf(void)
{
    return rx_buf;


int mrf24j_rx_datalength(void)
{
    return rx_info.frame_length - bytes_nodata;
}

void mrf24j_set_ignoreBytes(int ib)
{
    // some modules behaviour
    ignoreBytes = ib;
}


//Set bufPHY flag to buffer all bytes in PHY Payload, or not

void mrf24j_set_bufferPHY(uint8_t bp)
{
    bufPHY = bp;
}

uint8_t mrf24j_get_bufferPHY(void)
{
    return bufPHY;
}


//Set PA/LNA external control

void mrf24j_set_palna(uint8_t enabled)
{
    if (enabled)
    {
        mrf24j_write_long(MRF_TESTMODE, 0x07); // Enable PA/LNA on MRF24J40MB module.
    }else
    {
    	mrf24j_write_long(MRF_TESTMODE, 0x00); // Disable PA/LNA on MRF24J40MB module.
    }
}
*/


